'use strict'

var gulp = require('gulp');
var sass = require('gulp-sass');
var BrowserSync = require('browser-sync');


gulp.task('sass', function () {
    gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./css/*.scss', ['sass']);
});

gulp.task('browser-sync', function () {
    var files = ['./*.html', './css/*.css', './public/img/*.{png,jpg,gif}', './js/*.js']
    BrowserSync.init(files, {
        server: {
            baseDir: './'
        }
    });
});

gulp.task('default', ['browser-sync'], function(){
    gulp.start('sass:watch');
});