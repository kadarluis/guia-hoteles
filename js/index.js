$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2000
    });
    $('#exampleModal').on('show.bs.modal', function (e) {
        console.log('botón que activa el modal se inhabilite y cambie de color: btn-success');
        $('#botonTusDatos').removeClass();
        $('#botonTusDatos').addClass('btn btn-success');
        $('#botonTusDatos').prop('disabled', true);
    });
    $('#exampleModal').on('shown.bs.modal', function (e) {
        console.log('Modal se terminó de abrir')
    });
    $('#exampleModal').on('hide.bs.modal', function (e) {
        console.log('Modal comienza a ocultarse')
    });
    $('#exampleModal').on('hidden.bs.modal', function (e) {
        console.log('botón que activa el modal se vuelva a activar y pase al color original: btn-primary');
        $('#botonTusDatos').removeClass('');
        $('#botonTusDatos').addClass('btn btn-dark');
        $('#botonTusDatos').prop('disabled', false);
    });

});